package net.tncy.Hitblow;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {
    @BeforeEach
    public void setUpClass(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
    }

    public PersonTest(){}

    @Test
    public void test(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Person p = new Person("Jean","Oui");
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p);
        assertEquals(0,constraintViolations.size());
    }

    @Test
    public void wellDefined() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Calendar calendar = Calendar.getInstance();
        calendar.set(1995, Calendar.DECEMBER, 25, 4, 15, 20);
        Date birthDate = calendar.getTime();
        Person p = new Person("Jean","Oui",birthDate);
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p);
        assertEquals(0,constraintViolations.size());
    }

    @Test
    public void adult(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Calendar calendar = Calendar.getInstance();
        calendar.set(2005, Calendar.DECEMBER, 25, 4, 15, 20);
        Date birthDate = calendar.getTime();
        Person p = new Person("Jean","Oui",birthDate);
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p);
        assertEquals(0,constraintViolations.size());
    }

}